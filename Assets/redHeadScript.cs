﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class redHeadScript : MonoBehaviour
{
    private bool beginHitDetection;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
         
         Debug.Log(" Red head hit ");
         animator.SetBool("beenHit", true);
         
    }
}
