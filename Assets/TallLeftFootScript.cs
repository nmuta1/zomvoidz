﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TallLeftFootScript : MonoBehaviour
{
    private bool beginHitDetection;
    // Start is called before the first frame update
    void Start()
    {
        // trigger = GetComponent<Trigger>();
        // opponentTrigger = null;
        beginHitDetection = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if(beginHitDetection) {
           Debug.Log(" hit detected with ");
           Debug.Log(other);
        }
        if(beginHitDetection == false) {
            beginHitDetection = true; 
        }
    }
}
