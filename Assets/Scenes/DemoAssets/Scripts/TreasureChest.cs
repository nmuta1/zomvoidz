﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureChest : MonoBehaviour

{
    private Animator anim;
    private bool isOpen;
    

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        

	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Space)){
            isOpen = !isOpen;
            anim.SetBool("abre", isOpen);
        } 
	}

    void OnTriggerEnter(Collider other)
    {
        
    }

    void OnTriggerExit(Collider other)
    {
         
    }
}
