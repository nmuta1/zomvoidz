using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhinoScript : MonoBehaviour
{
    // know which items are clickable
    public CapsuleCollider rhinoCollider;
    public float moveSpeed = 5f;
 

    // swap cursor on per item basis
    // Start is called before the first frame update
    void Start()
    {
        rhinoCollider = GetComponent<CapsuleCollider>();
        rhinoCollider.height = 0.5f;
        rhinoCollider.center = new Vector3(0.3f, 0.3f, 0.3f);
     }

    // Update is called once per frame
    void Update()
    {
        float moveHoriz = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        
        Vector3 movement = new Vector3(moveHoriz, 0f, moveVertical);

        transform.Translate(movement * moveSpeed * Time.deltaTime);
        
    }
}
